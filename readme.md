# Cake Manager - Usman Arshad

Super serious cake management service developed as part of a Waracle assessment.

### Running

  - Clone the repository
  - Execute './gradlew :cake-service:bootRun' from the root directory
  - Access the service through port '7861'
  - GET '/cakes' from a client which expects application/json to receive a JSON list of cakes
  - GET '/cakes' from a client which expects application/htm to receive a HTML which contains a list of cakes formatted in a human readable format
  - POST '/cakes' to add a new cake
  - Navigate to the root directory '/' to see a human presentation of cakes (static html resources, forgive the vanilla JS, don't take this as an example of my JS frontend abilities, please refer to my last waracle assignment (frontend cake manage assessment) to see my abilities with frontend technologies :)
  - Navigate to '/addCakes.html' to add a cake without using the API i.e. 'It must be possible for a human to add a new cake'

### Requirements Breakdown

My understanding of the requires was as follows:

> By accessing the root of the server (/) it should be possible to list the cakes currently in the system.  This must be presented in an acceptable format for a human to read.

Initially I understood this as 'return text/plain' from the root path, however as I developed the service I felt an index.html page which presented more user-friendly HTML would be a better choice.

> It must be possible for a human to add a new cake to the server.

I have exposed an endpoint for add cakes (POST /cakes), however, the mention of a 'human' adding a new cake suggested that a simple endpoint won't suffice, thus I went down the route of adding a HTML page with a simple form that users can utilise to add cakes.

> By accessing an alternative endpoint (/cakes) with an appropriate client it must be possible to download a list of the cakes currently in the system as JSON data.

Straight away I saw this as a simple GET endpoint on '/cakes' to return JSON data, however, the mention of an appropriate client required me to think about things a bit deeper. From this requirement I realised I had to determine what the client was (a browser, another service). From my understanding, there is no fool proof way of achieving this as most methods can be spoofed (user-agent and accepts header), however, I ended up using 'accepts' header as I knew for a fact that pretty much all traditional browsers will supply this header.
In my code I don't do an exhaustive check to confirm what the user-agent or what the client accepts is, this is because I felt I just needed to demonstrate that I could distinguish between clients.

> Accessing the /cakes endpoint with a web browser must show the human presentation of the list of cakes.

Again, I interpreted 'human presentation' as either TEXT_PLAIN or TEXT_HTML, I originally coded towards TEXT_PLAIN however, later on I thought it would just be nicer to display HTML and took advantage of static resources in Spring Boot.

>The /cakes endpoint must also allow new cakes to be created.

I saw this as a simple POST to '/cakes'

### Reasoning and Justification

1) Why separate out the API and Service code?
If the API was to be exposed to a client we could just package up the API JAR and provide them with it. Additionally, I just like to separate out my API from service code.

2) Why so many layers?
Although this project isn't the best example of why you would split your microservice up into n-tiers, the point can still be made that separation of concerns is always king. In this case, if we had more heavy business logic we could keep it in the service layer without leaking into the repository or resource layer, thus keeping a clear separation of concerns. Additionally, In this case I used a silly 'DummyCakeRepository' that just hits an endpoint to get cakes, however, if I had the time I could simply replace the repository level with another implementation of 'CakeRepository' and the rest of my code wouldn't have to be touched.

3) Why serve static content?
Firstly, the requirement of serving a human readable result from the API just doesn't sit right with me, so whatever opportunity I had to pull that code out I took. I've only kept the 'human readable' response from the '/cakes' endpoint to demonstrate that I can distinguish between clients and serve content accordingly (... and it's part of the requirements), however, it's not something i'd ever thing of doing in my day job.

4) Why all the commented out config?
If I just had another hour I would of loved to of swapped out my dummy repository for an actual DB hosted in a docker container.

5) Again, please forgive the rushed frontend piece, time was very tight, please refer to my last waracle assessment to see my frontend abilities.
### Test

There wasn't much to test, so I just used this opportunity to demonstrate my style of testing (typically each test is broken down into 3 blocks of code, each representing, given, when, and then, respectively.
I believe i've demonstrated the ability to:

* Write unit tests with Mockito

* Write integration tests where required

* Use fixtures for deterministic results

### Further Work

Given the time I would implemented the following:

* Swagger config

* React or Angular Webapp instead of static resources, just to flex my full stack abilities

* Docker MariaDB instance, just so I could demonstrate swapping out repositories without affecting upper layers of code
