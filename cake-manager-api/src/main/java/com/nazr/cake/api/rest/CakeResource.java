package com.nazr.cake.api.rest;

import com.nazr.cake.api.model.Cake;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_HTML;

@Path("/")
public interface CakeResource {

    @GET
    @Produces({APPLICATION_JSON, TEXT_HTML})
    Response getCakes(@HeaderParam("Accept") String accepts);

    @POST
    @Consumes(APPLICATION_JSON)
    void addCake(Cake cake);
}
