document.addEventListener('DOMContentLoaded', loadCakes, false);

function loadCakes() {

    var cakeList = document.getElementById('cakeList');

    fetch('/cakes').then(result => result.json()).then(function renderCake(cakes){
        cakes.forEach(cake => {
            var node = document.createElement('div');
            node.innerHTML =
                '<div class="media">' +
                '   <div class="media-left media-middle">' +
                '       <a href="#"><img class="media-object" src="' + cake.image + '" height="100px" width="100px"></a>' +
                '  </div>' +
                '  <div class="media-body">' +
                '    <h4 class="media-heading">' + cake.title + '</h4>' +
                    cake.desc +
                '  </div>' +
                '</div>';

            cakeList.appendChild(node);
        })
    });
}