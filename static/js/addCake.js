function submitForm(e, form){
    e.preventDefault();

    var body = JSON.stringify({title: form.title.value, desc: form.description.value, image: form.imageUrl.value });

    console.log(body);
    fetch('/cakes', {
        method: 'post',
        body: body,
        headers: {
            'content-type': 'application/json'
        }
    }).then(function(response) {
        return response.status;
    }).then(function(data) {
        console.log('Added cake')
    }).catch(function(err) {
        console.log('Failed to add cake')
    });
}