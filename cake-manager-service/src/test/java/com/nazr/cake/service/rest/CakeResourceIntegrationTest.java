package com.nazr.cake.service.rest;

import com.nazr.cake.api.model.Cake;
import com.nazr.cake.api.rest.CakeResource;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.core.Response;
import java.util.List;

import static com.nazr.cake.service.fixtures.CakeFixtures.cake;
import static com.shazam.shazamcrest.MatcherAssert.assertThat;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CakeResourceIntegrationTest {

    @Autowired
    private CakeResource underTest;

    @Test
    public void shouldReturnCakeData_asJson_givenValidInput() {

        String accepts = APPLICATION_JSON;

        Response result = underTest.getCakes(accepts);
        List<Cake> cakes = (List<Cake>) result.getEntity();

        assertThat(result.getStatus(), is(200));
        assertThat(cakes.isEmpty(), is(not(true)));
    }

    @Test
    public void shouldReturnCakeData_asHtml_givenValidInput() {

        String accepts = TEXT_HTML;

        Response result = underTest.getCakes(accepts);
        String cakes = (String) result.getEntity();

        assertThat(result.getStatus(), is(200));

        // to check the html i'd probably use a html parser to confirm it's valid or compare it to a resource
        // however, for the sake of brevity i'm just going to check it has a html tag in it
        assertThat(cakes.isEmpty(), is(not(true)));
        assertThat(cakes.startsWith("<table"), is(true));
    }
}
