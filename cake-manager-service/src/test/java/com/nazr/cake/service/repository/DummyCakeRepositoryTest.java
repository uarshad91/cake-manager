package com.nazr.cake.service.repository;

import com.nazr.cake.api.model.Cake;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;

import java.util.List;

import static com.nazr.cake.service.fixtures.CakeFixtures.cake;
import static com.shazam.shazamcrest.MatcherAssert.assertThat;
import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DummyCakeRepositoryTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Client client;

    @InjectMocks
    private DummyCakeRepository underTest;

    @Test
    public void shouldRetrieveCakes() {

        when(client.target(anyString())
            .request()
            .accept(anyString())
            .get(any(GenericType.class)))
        .thenReturn(singletonList(cake()));

        List<Cake> cakes = underTest.retrieveCakes();

        assertThat(cakes.isEmpty(), is(not(true)));
        assertThat(cakes.size(), is(1));
    }

    @Test
    public void shouldPersistAndRetrieveCake_givenValidInput() {

        Cake cake = cake();

        underTest.persistCake(cake);
        Cake result = underTest.retrieveCake(cake.getTitle());

        assertThat(result, is(sameBeanAs(cake)));
    }
}
