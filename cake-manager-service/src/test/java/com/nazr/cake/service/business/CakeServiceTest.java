package com.nazr.cake.service.business;

import com.nazr.cake.api.model.Cake;
import com.nazr.cake.service.repository.CakeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.nazr.cake.service.fixtures.CakeFixtures.cake;
import static com.shazam.shazamcrest.MatcherAssert.assertThat;
import static java.util.Collections.singletonList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CakeServiceTest {

    @Mock
    private CakeRepository cakeRepository;

    @Mock
    private CakeResponseRenderer responseRenderer;

    @InjectMocks
    private CakeService underTest;

    @Test
    public void shouldGetCakesAsJson_givenValidInput() {

        String accepts = APPLICATION_JSON;

        when(cakeRepository.retrieveCakes()).thenReturn(singletonList(cake()));
        Object cakes = underTest.getCakes(accepts);

        verify(responseRenderer, never()).toHumanReadableForm(any());
        assertThat(cakes, instanceOf(List.class));
    }

    @Test
    public void shouldGetCakesAsString_givenValidInput() {

        String accepts = TEXT_HTML;
        List<Cake> dummyCakes = singletonList(cake());

        when(cakeRepository.retrieveCakes()).thenReturn(dummyCakes);
        when(responseRenderer.toHumanReadableForm(dummyCakes)).thenReturn("<html>someHtml</html>");
        Object cakes = underTest.getCakes(accepts);

        verify(responseRenderer, times(1)).toHumanReadableForm(any());
        assertThat(cakes, instanceOf(String.class));
    }
}
