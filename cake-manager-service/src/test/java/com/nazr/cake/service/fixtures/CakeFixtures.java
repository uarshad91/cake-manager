package com.nazr.cake.service.fixtures;

import com.nazr.cake.api.model.Cake;

public abstract class CakeFixtures {

    public static Cake cake() {
        return cake("someCake", "someDescription", "someUrl");
    }

    public static Cake cake(String title, String description, String imageUrl) {

        Cake cake = new Cake();
        cake.setTitle(title);
        cake.setDescription(description);
        cake.setTitle(imageUrl);

        return cake;
    }
}
