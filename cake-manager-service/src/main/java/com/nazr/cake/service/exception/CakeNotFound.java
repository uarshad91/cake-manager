package com.nazr.cake.service.exception;

public class CakeNotFound extends RuntimeException{

    public CakeNotFound(String message) {
        super(message);
    }
}
