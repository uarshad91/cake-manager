package com.nazr.cake.service.business;

import com.nazr.cake.api.model.Cake;
import com.nazr.cake.service.repository.CakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static javax.ws.rs.core.MediaType.TEXT_HTML;

@Component
public class CakeService {

    private CakeRepository cakeRepository;
    private CakeResponseRenderer responseRenderer;

    @Autowired
    public CakeService(CakeRepository cakeRepository, CakeResponseRenderer responseRenderer) {
        this.cakeRepository = cakeRepository;
        this.responseRenderer = responseRenderer;
    }

    public Object getHumanReadableCakes() {
        return responseRenderer.toHumanReadableForm(cakeRepository.retrieveCakes());
    }

    public Object getCakes(String accept) {

        if (consumerAcceptsHtml(accept)) {
            return getHumanReadableCakes();
        }

        return cakeRepository.retrieveCakes();
    }

    public void addCake(Cake cake) {
        cakeRepository.persistCake(cake);
    }

    private boolean consumerAcceptsHtml(String accept) {
        return accept.toLowerCase().contains(TEXT_HTML.toLowerCase());
    }
}
