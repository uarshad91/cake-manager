package com.nazr.cake.service.repository;

import com.nazr.cake.api.model.Cake;
import com.nazr.cake.service.exception.CakeNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Repository
public class DummyCakeRepository implements CakeRepository {

    private List<Cake> cakes = new ArrayList<>();

    private Client client;

    @Value("${dummy.repository.endpoint}")
    private String cakeEndpoint;

    @Autowired
    public DummyCakeRepository(Client client) {
        this.client = client;
    }

    @Override
    public void persistCake(Cake cake) {
        cakes.add(cake);
    }

    @Override
    public Cake retrieveCake(String title) {
        return cakes.stream()
                .filter(cake -> cake.getTitle().equalsIgnoreCase(title))
                .findAny()
                .orElseThrow(() -> new CakeNotFound(format("Cake with title '%s' not found", title)));
    }

    @Override
    public List<Cake> retrieveCakes() {

        if (cakes.isEmpty()) {
            cakes = client.target(cakeEndpoint)
                    .request()
                    .accept(APPLICATION_JSON)
                    .get(new GenericType<List<Cake>>() {});
        }

        return cakes;
    }
}
