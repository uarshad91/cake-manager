package com.nazr.cake.service.repository;

import com.nazr.cake.api.model.Cake;

import java.util.List;

public interface CakeRepository {

    void persistCake(Cake cake);

    Cake retrieveCake(String name);

    List<Cake> retrieveCakes();
}
