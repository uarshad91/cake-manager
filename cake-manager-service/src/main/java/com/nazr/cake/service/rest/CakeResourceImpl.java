package com.nazr.cake.service.rest;

import com.nazr.cake.api.model.Cake;
import com.nazr.cake.api.rest.CakeResource;
import com.nazr.cake.service.business.CakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Response;

@RestController
public class CakeResourceImpl implements CakeResource {

    private CakeService cakeService;

    @Autowired
    public CakeResourceImpl(CakeService cakeService) {
        this.cakeService = cakeService;
    }

    @Override
    public Response getCakes(String accept) {

        return Response.ok()
            .entity(cakeService.getCakes(accept))
            .build();
    }

    @Override
    public void addCake(Cake cake) {
        cakeService.addCake(cake);
    }
}
