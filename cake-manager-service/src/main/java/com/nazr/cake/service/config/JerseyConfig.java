package com.nazr.cake.service.config;

import com.nazr.cake.api.rest.CakeResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/cakes")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(CakeResource.class);
    }
}
