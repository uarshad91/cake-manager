package com.nazr.cake.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@EnableAutoConfiguration
public class CakeServiceMain {

    public static void main(String[] args) {
        SpringApplication.run(CakeServiceMain.class, args);
    }
}
