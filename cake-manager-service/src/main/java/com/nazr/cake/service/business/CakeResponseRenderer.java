package com.nazr.cake.service.business;

import com.nazr.cake.api.model.Cake;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CakeResponseRenderer {

    private static final String EMPTY_STRING = "";

    public String toHumanReadableForm(List<Cake> cakes) {

        //you didn't see this

        return "<table style=\"border-collapse: collapse; width: 100%%;\">" +
                "<tbody><tr style=\"height: 21px;\">" +
                "<td style=\"width: 33.3333%%; height: 21px;\"><strong>Title</strong></td>" +
                "<td style=\"width: 33.3333%%; height: 21px;\"><strong>Description</strong></td>" +
                "<td style=\"width: 33.3333%%; height: 21px;\"><strong>Image</strong></td>" +
                "</tr>" +
                cakes.stream()
                    .map(cake ->
                        String.format(
                            "<tr>" +
                                "<td style=\"width: 33.3333%%;\">%s</td>" +
                                "<td style=\"width: 33.3333%%;\">%s</td>" +
                                "<td style=\"width: 33.3333%%;\"><img src=\"%s\" alt=\"\" width=\"100\" height=\"100\" /></td>" +
                            "</tr>",
                            cake.getTitle(), cake.getDescription(), cake.getImageUrl()))
                    .reduce(String::concat).orElse(EMPTY_STRING) +
                "</tbody>" +
                "</table>";
    }
}
